<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Amministrazione docenti</title>
    <link rel="stylesheet" href="../../Home/image.css">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>
<body bgcolor="#f5f5dc">



<div>
    <br>
    <!--<div class="center"><img src="../assets/Scuola-guida-mascherina.png" alt="Mia immagine" style="..." class="center"</img></div>-->


    <?php
    require "../../config.php";
    require_once "../../authorized.php";
    verify('Admin');

    try {
        $stmt = $db-> prepare("
    SELECT I.id,P.patente, I.nome, I.cognome, C.giorno
    FROM iscritti I
    LEFT JOIN users U on I.id_users=U.id 
    LEFT JOIN patenti P ON P.id = I.id_patente
    LEFT JOIN corsi C ON I.id_patente= C.id_patente
    where role='Docente'
    ");

        $stmt->execute();
    }catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }

    ?>
    <div class="center"><h1>Amministrazione docenti</h1></div>
    <div class="center"><a href="add.php"><span class="material-icons">add_circle_outline</span></a></div>

    <br>
    <br>

    <div class="center">
        <table>
            <tr>
                <th>Patenti</th>
                <th>Docenti</th>
                <th>Corsi</th>
                <th></th>
            </tr>


            <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

                <tr>

                    <td><?= $row['patente'] ?></td>
                    <td><?= $row['nome'] ?> <?= $row['cognome'] ?></td>
                    <td><?= $row['giorno'] ?></td>

                    <td>
                        <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons">edit</span></button>
                        <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons">delete</span></button>
                    </td>
                </tr>
            <?php endwhile ?>

        </table>
    </div>
<script>
    function del(id) {
        if (confirm('Sei sicuro si voler eliminare questo docente?')) {
            location = "del.php?id=" + id ;
        }
    }

    function mod(id) {
        location = "edit.php?id=" + id;
    }
</script>
</body>
</html>