<?php

require_once "../../config.php";

#var_export($_POST);


$username = $_POST['username'] ?? '';
#$role = $_POST['Ruolo'] ?? '';
$password = $_POST['password'] ?? '';


try {
    $stmt = $db-> prepare("
    SELECT * FROM users U 
    left join iscritti I on I.id_users=U.id 
    WHERE username=:username  AND `password` = MD5(CONCAT(:password,:securitysalt)) 
    ");


    $stmt ->bindParam(':username',$username);
    #$stmt ->bindParam(':role',$role);
    $stmt ->bindParam(':password',$password);
    $stmt ->bindParam(':securitysalt',$securitysalt);
    $stmt->execute();



    if ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $_SESSION['user'] = $user;
        $_SESSION['username'] = $_POST['username'] ?? '';
        $_SESSION['msg'] = 'ok';



        if ($user['role']=='Admin'){
            header("location: /admin/amministrazione/admin.php");
        }elseif($user['role']=='Docente'){
            header("location: /admin/docente/docente.php");
        }else{
            header("location: /admin/studenti/studente.php");
        }
    } else if ($username == '' || $password ==''){
        $_SESSION['msg']= 'username or password are missing';
        $message = $_SESSION['msg'];
        echo "<script type='text/javascript'>alert('$message')</script>";
        header("refresh:0.1 ; url=login.php");
    }else {
        $_SESSION['msg']= 'username or password wrong';
        $message = $_SESSION['msg'];
        echo "<script type='text/javascript'>alert('$message')</script>";
        header("refresh:0.1 ; url=login.php");
    }

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}



?>


