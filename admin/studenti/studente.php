<?php
require_once "../../config.php";
require "../../config.php";
require_once "../../authorized.php";
verify('Docente' || 'Admin');

try {
    $stmt = $db-> prepare("
    SELECT p.patente as patente FROM iscritti I left join patenti p on p.id = I.id_patente 
    ");
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

$row = $stmt->fetch(PDO::FETCH_ASSOC);
#var_export($_SESSION);
#die();
?>
<html>
<head>
    <link rel="stylesheet" href="studente.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body class="profilo">

<div class="card" style="background-color: lavenderblush">
    <img src="../../assets/profilo2.png" style="width:80%;margin-top:45px; position: relative;left: 10px" >
    <div style="display: flex;">
        <div class="inserimento">
            <span class="material-icons" style="position: absolute; right: 438px;top:370px;z-index:1;">image</span><input id="image" name="imag" class="file" type="file" ><label class="inserisci">Inserisci immagine</label></input>
        </div>
        <span class="material-icons" style="position: relative; left: 29px; top: 25px;z-index:1">delete</span><button class="rimuovi" >Cancella l'immagine</button>
    </div>

    <h1><?php echo"Bentornato ".$_SESSION['username']?></h1>

    <p class="title">Studente</p>
    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">nome:</label>
        <h3 class="nome"><?= $_SESSION['user']['nome']?></h3>
    </div>

    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">Cognome:</label>
        <h3 class="cognome"><?= $_SESSION['user']['cognome']?></h3>
    </div>

    <div style="display: flex;flex-direction: row; align-items: center">
        <label style="margin-right: 150px">Patente:</label>
        <h3 class="patente"><?= $row['patente']?></h3>
    </div>

</div>


</body>
</html>

