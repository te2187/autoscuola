<?php
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin');
#var_export($_SESSION['user']);
?>
<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" a href="amministrazione.css"/>
    <title>Admin</title>
</head>
<body>
<a href="../../index.php"><span class="material-icons" style="float: right; color: orangered;">home</span></a>

    <div class="username">
    <h1><?php echo 'Welcome '.$_SESSION['username']; ?></h1>
        <a href="../logout.php"><span  class="material-icons logout">logout</span></a>

    </div>
<div class="container" >
    <img src="../../assets/docenti.png">
</div>
<a href="/admin/ad_docente/ad_docente.php"><input class="btn-Enter" type="submit" value="Amministrazione Docenti"/></a>
<div class="container2">
    <img src="../../assets/studenti.png">
</div>
<a href="/admin/ad_studente/ad_studente.php"><input class="btn-Enter2" type="submit" value="Amministrazione Studenti"/></a>

</body>
</html>
