<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Amministrazione docenti</title>
    <link rel="stylesheet" href="../../Home/image.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">



</head>
<body bgcolor="#f5f5dc">

<div>
    <br>
    <!--<div class="center"><img src="../assets/Scuola-guida-mascherina.png" alt="Mia immagine" style="..." class="center"</img></div>-->


    <?php
    require_once "../../config.php";
    require_once "../../authorized.php";
    verify('Admin');

    try {

        $stmt = $db-> prepare("
    SELECT I.id,I.nome,I.cognome,I.id_patente,P.patente
    FROM iscritti I 
    left join users U on U.id=I.id_users 
    left join patenti P on I.id_patente=P.id
    where role='Studente'
    ");


        $stmt->execute();
        #$stmtb->execute();
    }catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }

    ?>
    <div class="center"><h1>Amministrazione Studenti</h1></div>
    <div class="center"><a href="add.php"><span class="material-icons">add_circle_outline</span></a></div>

    <br>
    <br>

    <div class="center">
        <table>
            <tr>
                <th>id</th>
                <th>Studenti</th>
                <th>Patenti</th>
                <th></th>
            </tr>


            <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

                <tr>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['nome'] ?> <?= $row['cognome'] ?></td>
                    <td><?= $row['patente'] ?></td>


                    <td>
                        <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons">edit</span></button>
                        <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons">delete</span></button>
                    </td>
                </tr>
            <?php endwhile ?>

        </table>
    </div>
    <script>
        function del(id) {
            if (confirm('Sei sicuro si voler eliminare questo docente?')) {
                location = "del.php?id=" + id ;
            }
        }

        function mod(id) {
            location = "edit.php?id=" + id;
        }
    </script>
</body>
</html>