<?php
require_once "../../config.php";
require "../../config.php";

$stmt = $db->prepare("
            SELECT * from patenti;
            ");
$stmt->execute();
?>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="register.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<div class="container">
    <img src="../../../assets/user.png"/>
    <form method="post" action="register_r.php">
        <div class="form_input">
        <input type="text" placeholder="Enter Your username" name="username" size="30">
        </div>
        <br>

        <div class='docente' id="Docente" style="display: none;">
            <div class="form_input" class="docente" id="Docente">
            <input type="text" placeholder="Enter Your name" name="nome_docente" size="30">
            </div>
            <br>
            <br>
            <div class="form_input">
            <input  type="text" placeholder="Enter Your surname" name="cognome_docente" size="30">
            </div>
            <br>
            <br>
            <!--<input type="text" placeholder="Enter driving license taught" name="corso" size="30"-->
            <table>
                <tr>
                    <td>Patente:</td>
                    <td>
                        <select name="id_patente" id="id_patente">
                            <option selected hidden>Seleziona una patente</option>
                            <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
                                <option value="<?= $row['id'] ?>"><?= $row['patente'] ?></option>
                            <?php endwhile ?>
                        </select>
                    </td>
                </tr>
            </table>


        </div>
        <div id="Studente" class="studente" style="display: none;">
            <div class="form_input">
            <input type="text" placeholder="Enter Your name" name="nome_studente" size="30">
            </div>
            <br>
            <div class="form_input">
            <input name="cognome_studente" type="text" placeholder="Enter Your surname" size="30">
            </div>
        </div>

        <table>
            <tr>
                <td>Ruolo:</td>
                <td>
                    <select name="Ruolo">
                        <option value="main" selected onclick="show_box('select role')"> select role</option>
                        <option value="Admin" onclick="show_box('Admin')">Admin</option>
                        <option value="Docente" id="option_docente" onclick="show_box('Docente')">Docente</option>
                        <option value="Studente" onclick="show_box('Studente')">Studente</option>
                    </select>
                </td>
            </tr>
        </table>
        <br>
        <div class="form_input">
        <input type="password" placeholder="Enter Your Password" name="password"  size="30">
        </div>
        <br>


        <br>
        <input class="btn-register" type="submit" value="Register">
        <br><br>
        <label class="text" style="font-size: 18px"><a class="link" href="../login/login.php">Hai gia un account?</a></label>

    </form>
    </div>
    <script src="../../js/main.js"></script>
</html>


