<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Autoscuola Bararu</title>
    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon-32x32.png">
    <link rel="stylesheet" href="Home/image.css">
    <link rel="stylesheet" href="animazioni/animazione.css">
    <link rel="stylesheet" href="Home/nav.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

    <div class="animazione">
        <span class="text1">Benvenuto nella</span>
        <span class="text2">AutoScuola Bararu</span>
    </div>

    <div class="container">
        <div class="inizio">
        <header id="prova">
            <img src="assets/barulogo2.png" alt="Logo" width="260px" class="logo"/>
            <nav>
                <ul class="menu">
                    <li><a class="Service" href="index.php">Home</a></li>
                    <li><a class="Service" href="Home/servizi.php">Servizi</a></li>
                    <li><a class="Service" href="#">Quiz Patente</a></li>
                    <li><a class="Service" href="Home/Contattaci.php">Contattaci</a></li>
                </ul>
            </nav>
            <div class="mauro">
                <a href="admin/login/login.php"><button class="Contact">Login</button></a>
                <a href="admin/register/register3.php"><button class="Contact">Register</button></a>
            </div>
        </header>
        </div>
        <br>
        <br>
        <br>
        <br>

<?php
require_once "config.php";

try {
    $stmt = $db-> prepare("
    SELECT I.id,P.patente, I.nome, I.cognome, C.giorno
    FROM iscritti I
    LEFT JOIN patenti P ON P.id = I.id_patente
    LEFT JOIN corsi C ON I.id_patente= C.id_patente
    ");

    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


?>

<div class="center"><h1>Autoscuola Bararu</h1></div>
        
<br>
<br>

<div class="center">
    <table>
        <tr>
            <th>Patenti</th>
            <th>Docenti</th>
            <th>Corsi</th>
        </tr>


        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>

            <tr>
                <td><?= $row['patente'] ?></td>
                <td><?= $row['nome'] ?> <?= $row['cognome'] ?></td>
                <td><?= $row['giorno'] ?></td>


            </tr>
        <?php endwhile ?>

    </table>
</div>
    </div>


</body>
</html>