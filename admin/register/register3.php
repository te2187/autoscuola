<?php
require_once "../../config.php";
require "../../config.php";

$stmt = $db->prepare("
            SELECT * from patenti;
            ");
$stmt->execute();
?>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="register.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<div class="container">
    <img src="../../../assets/user.png"/>
    <form method="post" action="register_r2.php" class="form">

        <div class="form_input username">
            <span class="material-icons" id="omino" >person</span>
            <input type="text" placeholder="Enter Your username" name="username" size="30">
        </div>
        <br>
        <div class="form_input username">
            <span class="material-icons badge1">badge</span>
            <input type="text" placeholder="Enter Your name" name="nome_studente" size="30">
        </div>
        <br>
        <div class="form_input username">
            <span class="material-icons badge2">badge</span>
            <input name="cognome_studente" type="text" placeholder="Enter Your surname" size="30">
        </div>

        <div class="form_input password">
            <span class="material-icons" id="lucchetto">lock</span>
            <input type="password" placeholder="Enter Your Password" name="password"  size="30">
        </div>
        <br>


        <br>
        <input class="btn-register" type="submit" value="Register">
        <br><br>
        <br><br>
        <label class="text" style="font-size: 18px"><a class="link" href="../login/login.php">Hai gia un account?</a></label>

    </form>
</div>
<script src="../../js/main.js"></script>
</html>


