<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="96x96" href="../../assets/favicon-32x32.png">
    <title>Nuovo Docente</title>
    <style>
        label {
            width: 4pc;
            display: inline-block;
        }

        input[value="Salva"] {
            background-color: yellowgreen;

        }

        input[type=submit], input[type=button], input[type=reset] {
            cursor: pointer;
            border: 1px solid #4444;
            border-radius: 2px;
        }
    </style>
</head>
<body>

<?php
require "../../config.php";
require_once "../../config.php";
require_once "../../authorized.php";
verify('Admin');
try {
    $stmta = $db-> prepare("SELECT * FROM patenti");
    $stmta->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $msg = $_SESSION['add_data']['msg'];
    $username = $_SESSION['add_data']['username'];
    $nome = $_SESSION['add_data']['nome'];
    $cognome = $_SESSION['add_data']['cognome'];
    #$patente = $_SESSION['add_data']['patente'];
    $password = $_SESSION['add_data']['password'];
    unset($_SESSION['add_data']);

} else {
    $msg = '';
    $username = '';
    $nome = '';
    $cognome = '';
    #$patente = '';
    $password = '';
}
?>

<h2>Nuovo Docente</h2>

<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>
<br>
<form method="post" action="add_r.php" enctype="multipart/form-data">


    <label for="username">Username</label>
    <input id="username" type="text" name="username" size="30" maxlength="255" value="<?= $username ?>">
    <br><br>

    <label for="nome">Nome</label>
    <input id="nome" type="text" name="nome" size="30" maxlength="255" value="<?= $nome ?>">
    <br><br>


    <label for="cognome">Cognome</label>
    <input id="cognome" type="text" name="cognome" size="30" maxlength="255" value="<?= $cognome ?>">
    <br><br>

    <table>
        <tr>
            <td>Patente:</td>
            <td>
                <select name="id_patente" id="id_patente">
                    <option selected hidden>Seleziona una patente</option>
                    <?php while($row = $stmta->fetch(PDO::FETCH_ASSOC)): ?>
                        <option value="<?= $row['id'] ?>"><?= $row['patente'] ?></option>
                    <?php endwhile ?>
                </select>
            </td>
        </tr>
    </table>
    <br>


    <label for="password">Password</label>
    <input id="password" type="password" name="password" size="30" maxlength="255" value="<?= $password ?>">
    <br><br>


    <input type="button" value="Annulla" onclick="history.back()">
    <input type="reset">
    <input type="submit" value="Salva">


</form>

</body>
</html>