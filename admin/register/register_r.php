<?php

require_once "../../config.php";

var_export($_POST);

$username = $_POST['username'] ?? '';
$role = $_POST['Ruolo'] ?? '';
$password = $_POST['password'] ?? '';
$nome_docente= $_POST['nome_docente'] ?? '';
$cognome_docente= $_POST['cognome_docente'] ?? '';
$nome_studente= $_POST['nome_studente'] ?? '';
$cognome_studente= $_POST['cognome_studente'] ?? '';
$id_patente = $_POST['id_patente'] ?? 0;



try {
    $stmt = $db->query("SELECT username FROM users WHERE username = '$username' ");
    $user=$stmt->fetch(PDO::FETCH_ASSOC );


    if ($user['username']==$username){
        $message = "Utente gia esistente";
        echo "<script type='text/javascript'>alert('$message')</script>";
        header("refresh:0.1 ; url=register.php");

    }

    if($role=='main'){
        $message = "Seleziona un ruolo";
        echo "<script type='text/javascript'>alert('$message')</script>";
        header("refresh:0.1 ; url=register.php");
    }
    else if($role=='Admin') {
        if ($username=='' || $password=='') {
            $message = "Completare tutti i campi";
            echo "<script type='text/javascript'>alert('$message')</script>";
            header("refresh:0.1 ; url=register.php");
        }else{
            $stmt = $db->prepare("
            INSERT INTO users(username,`role` ,password) values (:username,:role,MD5(CONCAT(:password,:securitysalt))) 
        ");
        }
    }

    elseif($role=='Docente'){

        if ($username=='' || $password=='' || $nome_docente=='' || $cognome_docente=='' || $id_patente=='') {
            $message = "Completare tutti i campi";
            echo "<script type='text/javascript'>alert('$message')</script>";
            #echo 'elseif(Docente)';
            #die();
            header("refresh:0.1 ; url=register.php");
        }else{
            $stmt = $db->prepare("
            INSERT INTO users(username,`role` ,password) values (:username,:role,MD5(CONCAT(:password,:securitysalt))) 
            ");
            $stmt2 = $db->prepare("
            INSERT INTO docente(nome,cognome,id_patente) values (:nome,:cognome,:id_patente) 
            ");
            $stmt2 ->bindParam(':nome',$nome_docente);
            $stmt2 ->bindParam(':cognome',$cognome_docente);
            $stmt2 ->bindParam(':id_patente',$id_patente);
            $stmt2->execute();
        }

    }
    elseif ($role=='Studente') {
        #echo 'elseif(Studente)';
        #die();
        if ($username=='' || $password=='' || $nome_studente=='' || $cognome_studente=='') {
            $message = "Completare tutti i campi";
            echo "<script type='text/javascript'>alert('$message')</script>";
            header("refresh:0.1 ; url=register.php");
        }else{

            $stmt = $db->prepare("
            INSERT INTO users(username,`role` ,password) values (:username,:role,MD5(CONCAT(:password,:securitysalt))) 
            ");
            $stmt3 = $db->prepare("
            INSERT INTO studenti(nome,cognome) values (:nome,:cognome)
            ");
            $stmt3 ->bindParam(':nome',$nome_studente);
            $stmt3 ->bindParam(':cognome',$cognome_studente);
            $stmt3->execute();
        }

    }
    $stmt ->bindParam(':username',$username);
    $stmt ->bindParam(':role',$role);
    $stmt ->bindParam(':password',$password);
    $stmt ->bindParam(':securitysalt',$securitysalt);
    $stmt->execute();
    header("location:/login/login.php");

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


?>





