<?php

require "../../config.php";
require_once "../../config.php";

#var_export($_POST); die;

$nome = $_POST['nome'] ?? '';
$cognome = $_POST['cognome'] ?? '';
$id_patente = $_POST['id_patente'] ?? '';
$giorno = $_POST['giorno'] ?? '';
$id = $_POST['id'] ?? 0;
$id_docenti = $_POST['id_docenti'] ?? 0;

if ($id == '') $id = 0;
//var_dump($year);

if ($nome == '') {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'nome' => $nome ,
        'cognome' => $cognome,
        'id_patente' => $id_patente,
        'giorno' => $giorno
    ];
    header('location: /admin/ad_docente/edit.php?');
    die;
}


try {

    $stmt = $db-> prepare("
       UPDATE iscritti SET
        nome = :nome,
        cognome= :cognome,
        id_patente = :id_patente
        where id = :id
    ");
    $stmtb = $db-> prepare("
       update corsi set 
        giorno = :giorno
        where id_docenti= :id_docenti
    ");

    $stmt->bindParam(':nome', $nome);
    $stmt->bindParam(':cognome', $cognome);
    $stmt->bindParam(':id_patente', $id_patente);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $stmtb->bindParam(':giorno', $giorno);
    $stmtb->bindParam(':id_docenti', $id_docenti);
    $stmtb->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/ad_docente/ad_docente.php');

?>



