<?php
require_once "../../config.php";
if(!isset($_SESSION['backto'])){
    $backto=$_SERVER['HTTP_REFERER'] ?? '';
    if($backto==''){
        $backto='login.php';
    }
    $_SESSION['backto']=$backto;
}
echo $_SESSION['username'];
?>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="login.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>
<a href="../../index.php"><span class="material-icons" style="float: right; color: orangered;">home</span></a>

<div class="container">
    <img src="../../../assets/user.png"/>
    <form method="post" action="login_r.php" class="form">
        <div class="form_input username">
            <span class="material-icons" id="omino" >person</span>
            <input class="field" type="text" name="username" placeholder="Enter Your Username" size="30" >
        </div>
        <br>
        <div class="form_input username">
            <span class="material-icons" id="lucchetto">lock</span>
            <input class="field" type="password" name="password" placeholder="Enter Your Password" size="30">
        </div>
        <br>

        <input class="btn-login" type="submit" value="Login"/>
        <br><br>
        <br><br>
        <label class="text" style="font-size: 18px"><a class="link" href="../register/register3.php">Non hai ancora un account?</a></label>
    </form>
</div>
</body>
</html>

